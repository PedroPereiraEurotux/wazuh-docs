# Agentless monitoring

In order to connect the manager to the device using SSH authentication, the following `/var/ossec/register_host.sh` script should be used.

Using the `add` option agents may be registered.
```bash
/var/ossec/agentless/register_host.sh add root@example_address.com example_password [enablepass]
```

To connect with public key encription use `NOPASS` instead of a password.
`enablepass` can be used to specify the enable password for Cisco devices.

# [Configuring agentless monitoring](https://documentation.wazuh.com/current/user-manual/reference/ossec-conf/agentless.html#reference-ossec-agentless)
After establishing connection the agentless configuration needs to be added to the manager.

## Options:
- type: specifies if a syscheck is run (`ssh_integrity_check_bsd` or `ssh_integrity_check_linux`), or a diff between comands (`ssh_generic_diff` or `ssh_pixconfig_diff`)
- frequency: time between checks in seconds
- host: username and host
- state: 
- - `periodic`: output rom each check is analysed with the rulesed as if from a monitored log
- - `periodic_diff`: Output from each agentless check is compared to the output of the previous run. Changes are alerted on, similar to file integrity monitoring.
- arguments: 
- - for Integrity checks this is a space-delimited list of files or directories to be monitored . 
- - for generic diff this is a `;` delimite list of commands

sample config:
```xml
<agentless>
  <type>ssh_integrity_check_linux</type>
  <frequency>300</frequency>
  <host>admin@192.168.1.108</host>
  <state>periodic_diff</state>
  <arguments>/etc /usr/bin /usr/sbin</arguments>
</agentless>
```
this config sends an alert if any of the directiries change

```xml
<agentless>
  <type>ssh_generic_diff</type>
  <frequency>20000</frequency>
  <host>root@test.com</host>
  <state>periodic_diff</state>
  <arguments>ls -la /etc; cat /etc/passwd</arguments>
</agentless>
```
An alert will be triggered if the output of the commands changes.

Since agentless monitoring cant be associated with a group, a query can be created that agregates all agents inn a given group by finding the label, and eatch agentless that would be added to that group.
This query can be saved to wazuh to have a consistent way to check on all machines of any given company for ex.

```json
{
  "query": {
    "bool": {
      "should": [
        { "match": { "agent.labels.empresa": "Empresa-A"  } },
        { "match": { "agentless.host": "root@test1.com" } },
        { "match": { "agentless.host": "root@test2.com" } },
                                ...
        { "match": { "agentless.host": "root@testN.com" } },
      ],
      "minimum_should_match": 1
    }
  }
}
```