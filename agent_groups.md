# Creating Agent groups

Only the master server can magane users. The binary `/var/ossec/bin/agent_groups` is responsible for all the group creation and management duties.
By default every user is a member of the group `default`.
For each group the user is in, that goups files are melded into one and sent to the client. If two groups have contradicting configurations, the groupt that was added last to the agent "wins". This only applies to the specific parts that disagree.

The manager can be interacted with directly though the binary or through the API with the wazuh dashboard.
For more information see the [Oficial Documentation](https://documentation.wazuh.com/current/user-manual/agents/grouping-agents.html)

## Using the Binary

To create a group run:
```shell
/var/ossec/bin/agent_groups -a -g group-name
```

To add an user to a group run:
the user id should be a sequence of 3 digits ex.: `002`
```shell
/var/ossec/bin/agent_groups -a -i user-id -g group-name
```

To list all users and their ID's:
```shell
/var/ossec/bin/manage_agents -l
```

To avoid the `y/N` prompt use the option `-q`

for more information on the `agent_groups` binary:
```shell
/var/ossec/bin/agent_groups --help
```

# Using the Dashboard
In the dashboard click the drop down menu in the uper left corner, the select Management > Administration > Groups
Here Use `Add New Group` in the top right to create a new group
To manage a group click the group in question click it in the groups list, in the new page use the `Manage Agents` button.
To edit the group file open the `Files` tab.

# Creating the shared config

After the group is created a directory with that groups file is created automaticly in `/var/ossec/etc/shared`
To make config file that affect all users in a group, edit the file `/var/ossec/etc/shared/group-name/agent.conf` in the manager, or the `agent.conf` file in th `Files` tab of that user group in the dashboard.
The first step after creating a user group should be adding a label to that group's agent configs. This was when using the dashboard the label can uniquely identify every agent in that agent group.

- To add a label:
```xml
<agent_config>
    ...
    <labels>
        <label key="group-name-example">companyA-prod-machines</label>
        <label key="example-key">key-value</label>
    </labels>
    ...
</agent_config>
```

- To monitor a directory:
```xml
<agent_config>
    ...
    <syscheck>
        <directory>/path/to/dir</directory>
    </syscheck>
    ...
</agent_config>
```

- Add real time monitoring
```xml
<agent_config>
    ...
    <syscheck>
        <directory>/path/to/dir realtime="yes"</directory>
    </syscheck>
    ...
</agent_config>
```

- Agent specific setting in the shared config
instead of editing the agent's `ossec.conf`
```xml
<agent_config name=”agent01”>
...
<agent_config os="Linux">
...
<agent_config profile="UnixHost">
```
Alternatively the configuration can be added directly to the agent's `ossec.conf` file



For more information see:
- [Agent configuration](https://documentation.wazuh.com/current/user-manual/reference/ossec-conf/index.html)
- [Syscheck options](https://documentation.wazuh.com/current/user-manual/reference/ossec-conf/syscheck.html) for more information on how to configure the syscheck