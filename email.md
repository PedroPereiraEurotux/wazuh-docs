# [Email notifications](https://documentation.wazuh.com/current/user-manual/manager/manual-email-report/index.html)

An alert can trigger an aler in 3 ways.

## Generic Email Options

In order to configure Wazuh to send email alers the email setting must first be configured in the `<global>` [section](https://documentation.wazuh.com/current/user-manual/reference/ossec-conf/global.html) of the `ossec.conf`

```xml
<ossec_config>
    <global>
        <email_notification>yes</email_notification>
        <email_to>me@test.com</email_to>
        <smtp_server>mail.test.com</smtp_server>
        <email_from>wazuh@test.com</email_from>
    </global>
</ossec_config>
```

After thos is done, the `<alerts>` [section](https://documentation.wazuh.com/current/user-manual/reference/ossec-conf/alerts.html#reference-ossec-alerts) can be used to configured with `email_alert_level`. This sets the minimum alert level that will trigger an email, default value is 12.

## granular email options

The the `email_alerts` tag can provide more granular configurations of the email alerts.

```xml
<email_alerts>
  <email_to>you@example.com</email_to>
  <group>pci_dss_10.6.1,</group>
  <rule_id>515, 516</rule_id>
  <level>4</level>
  <do_not_delay />
</email_alerts>
```

this would send an email to `you@example.com` when all of th econditions below are met:

- the agent belong to the groupt `pci_dss_106.1`
- the rule that matched was either `515,516`
- the level of the rule is at least 4 ( this is ignored if the minimum alert level is set to a higher value)

## Force forwarding

To force an email to be triggered on a specific rule,the rule itself can be set to send an email with `<option>alert_by_email</options>`. This ignores the minimum alert level.

```xml
<rule id="502" level="3">
  <if_sid>500</if_sid>
  <options>alert_by_email</options>
  <match>Ossec started</match>
  <description>Ossec server started.</description>
</rule>
```
