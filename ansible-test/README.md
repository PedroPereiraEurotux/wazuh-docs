# How to use

UNTESTED RIGHT NOW
the latest changes were not tested

## deploy-agent

This role install and configures an agent

The agents and their agent specific configurations can be set in the inventory file `inventory/inv.yml`
ex:

```yml
all:
  hosts:
    test-agent:
      ansible_host: 10.10.4.214
      registration_agent_name: "Agent-name"
      registration_groups: "default,test-group1"

    test-agent2:
      ansible_host: 10.10.4.215
      registration_agent_name: "Agent-name2"
      registration_groups: "default,grupo-teste2"
vars:
```

The only value that must be set is the host:`ansible_host`. Every other value can assume a default.

Every other configurable value is already on the inv.yml file, only commented. Uncoment and inzert the desired value.
These changes apply to all hosts, to alter only one hosts value, insert that variable deffinition on that hosts part of the config.
