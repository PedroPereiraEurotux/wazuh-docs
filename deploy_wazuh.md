# Wazuh

## Index

## Deployment

- [single machine](#single mahcine)
- [3 machine guide](#3-machines)
- [replicated guide](#replicated)
- [Clients](#clients)

[Configuration](#configuration)

- [Shared](#main-configurations-options)
- [Client](#client-specific-configuation)
- [Active Response](#active-response)

Wazuh's official instalation guide: `https://documentation.wazuh.com/current/installation-guide/index.html`

The wazuh server is composed of 3 componentes, the Indexer, the Server and the Dashboard
These can be instaled in the same or diferent machines.
This guide will have 3 versious, [[single machine](#single mahcine)] where one machines hosts all 3 componens; a [3 machine guide](#3-machines) where one machine hosts one service; the [replicated guide](#replicated) where the replication capabilities of the app are taken advantage of by using multiple machines for the server and indexer.

## 3 machines

network example:

```txt
Dashbord -server -- | - client1
    |       |       | - client2
    +--- indexer -- | - client3

indexer   : 10.0.0.10
server    : 10.0.0.20
dashboard : 10.0.0.30

clientsN  : 10.0.0.N
```

## Indexer

### Install

These steps should be run in indexer machine

- Download the installation assistant and configuration file

```shell
curl -sO https://packages.wazuh.com/4.3/wazuh-install.sh
curl -sO https://packages.wazuh.com/4.3/config.yml
```

- Edit the config file `config.yml` and replace the node ips
Ex.: config

```txt
nodes:
  indexer:
    - name: node-1
      ip: 10.0.0.10
  server:
    - name: wazuh-1
      ip: 10.0.0.20
  dashboard:
    - name: dashboard
      ip: 10.0.0.30
```

- run the downloaded script with `--generate-config-files` to generate the cluster config files/passwords/keys
- this opperations needs write permissions to `/var/log` so run as su with `sudo`

```shell
bash wazuh-install.sh --generate-config-files
```

- copy `wazuh-install-files.tar` to every node that will be an inder/server/dashboard. The file will be created with a low permisison level (600) so we need to copy it as root.
**[Note]** since the user `root` has its own home, its ssh key related files ate stored in /home/.ssh . If using scp be sure that the user root can use `ssh` to log in to the target machines ( in this case, the server and dashbord).
**[Note]** The `.tar` file should not user readable since it contains sensitive information.

- run the downloaded script with `--wazuh-indexer <node-name>`, for the example config this would be
- this needs to be run with su permission, so run with `sudo`

```shell
sudo bash wazuh-install.sh --wazuh-indexer node-1
```

- start the cluster

```shell
sudo bash wazuh-install.sh --start-cluster
```

### Testing

```shell
tar -axf wazuh-install-files.tar wazuh-install-files/wazuh-passwords.txt -O | grep -P "\'admin\'" -A 1
```

the above command fetches the admin password,
replace `<ADMIN_PASSWORD>` by the password just fetched and `<WAZUH_INDEXER_IP>` by the server ip.

```shell
curl -k -u admin:<ADMIN_PASSWORD> https://<WAZUH_INDEXER_IP>:9200
```

using the example config this would be

```shell
curl -k -u admin:<ADMIN_PASSWORD> https://10.0.0.10:9200
```

- this should result in somehting like this:

```json
{
  "name" : "node-1",
  "cluster_name" : "wazuh-cluster",
  "cluster_uuid" : "cMeWTEWxQWeIPDaf1Wx4jw",
  "version" : {
    "number" : "7.10.2",
    "build_type" : "rpm",
    "build_hash" : "e505b10357c03ae8d26d675172402f2f2144ef0f",
    "build_date" : "2022-01-14T03:38:06.881862Z",
    "build_snapshot" : false,
    "lucene_version" : "8.10.1",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "The OpenSearch Project: https://opensearch.org/"
}
```

## Server

### Install

For the server the only thing needed is to download the install script and run it
**[Note]** the `wazuh-install-files.tar` needs to be copied to the Server if it hasn't been copied before
The node name needs to be the same as configured in config.yml.
For the example config it would be `wazuh-1` as used bellow

```shell
curl -sO https://packages.wazuh.com/4.3/wazuh-install.sh
bash wazuh-install.sh --wazuh-server wazuh-1
```

## Dashboard

### Install

for the dashboard we have the same considerations as with the server, so do the same steps but when running the install script run with `--wazuh-dashboard` and the dashboard node name.

```shell
curl -sO https://packages.wazuh.com/4.3/wazuh-install.sh
sudo bash wazuh-install.sh --wazuh-dashboard dashboard
```

after this is done the bellow text should be printed. The `<ADMIN_PASSWORD>` can be used to log in to the wazuh dashboard.

```
INFO: --- Summary ---
INFO: You can access the web interface https://<wazuh-dashboard-ip>
   User: admin
   Password: <ADMIN_PASSWORD>

INFO: Installation finished.
```

This password is stored in the `wazuh-passwords.txt` file in `wazuh-install-files.tar`.It can also be seen by running

```shell
tar -O -xvf wazuh-install-files.tar wazuh-install-files/wazuh-passwords.txt
```

### Testing

We can now test the whole setup by logging in to the dashboard.
Access it by going to `https:<dashboard-ip>` and using the admin credentials seen above

# Single machine

```
network example:

indexer/sever/dashboard - | - client1
                          | - client2
                          | - client3

indexer   : 10.0.0.10
server    : 10.0.0.10
dashboard : 10.0.0.10

clientsN  : 10.0.0.N
```

## Indexer

### Install

- Download the assistant and configuration file

```shell
curl -sO https://packages.wazuh.com/4.3/wazuh-install.sh
curl -sO https://packages.wazuh.com/4.3/config.yml
```

- Edit the config file `config.yml` and replace the node ips
Ex.: config

```
nodes:
  indexer:
    - name: node-1
      ip: 10.0.0.10
  server:
    - name: wazuh-1
      ip: 10.0.0.10
  dashboard:
    - name: dashboard
      ip: 10.0.0.10
```

- run the downloaded script with `--generate-config-files` to generate the cluster config files/passwords/keys
- this opperations needs write permissions to `/var/log` so run as su with `sudo`

```shell
bash wazuh-install.sh --generate-config-files
```

- run the downloaded script with `--wazuh-indexer <node-name>`, for the example config this would be
- this needs to be run with su permission, so run with `sudo`

```shell
sudo bash wazuh-install.sh --wazuh-indexer node-1
```

- start the cluster

```shell
sudo bash wazuh-install.sh --start-cluster
```

### Testing

```shell
tar -axf wazuh-install-files.tar wazuh-install-files/wazuh-passwords.txt -O | grep -P "\'admin\'" -A 1
```

the above command fetches the admin password,
replace `<ADMIN_PASSWORD>` by the password just fetched and `<WAZUH_INDEXER_IP>` by the server ip

```shell
curl -k -u admin:<ADMIN_PASSWORD> https://<WAZUH_INDEXER_IP>:9200
```

- this should result in somehting like this:

```json
{
  "name" : "node-1",
  "cluster_name" : "wazuh-cluster",
  "cluster_uuid" : "cMeWTEWxQWeIPDaf1Wx4jw",
  "version" : {
    "number" : "7.10.2",
    "build_type" : "rpm",
    "build_hash" : "e505b10357c03ae8d26d675172402f2f2144ef0f",
    "build_date" : "2022-01-14T03:38:06.881862Z",
    "build_snapshot" : false,
    "lucene_version" : "8.10.1",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "The OpenSearch Project: https://opensearch.org/"
}
```

## Server

### Install

For the server the only thing needed is to run the install script
The node name needs to be the same as configured in config.yml.

- run the downloaded script with `--wazuh-indexer <node-name>`, for the example config this would be

```shell
bash wazuh-install.sh --wazuh-server wazuh-1
```

## Dashboard

### Install

for the dashboard we have the same considerations as with the server, so do the same steps but when running the install script run with `--wazuh-dashboard <node-name>`.

```shell
curl -sO https://packages.wazuh.com/4.3/wazuh-install.sh
sudo bash wazuh-install.sh --wazuh-dashboard dashboard
```

after this is done the bellow text should be printed. The `<ADMIN_PASSWORD>` can be used to log in to the wazuh dashboard.

```
INFO: --- Summary ---
INFO: You can access the web interface https://<wazuh-dashboard-ip>
   User: admin
   Password: <ADMIN_PASSWORD>

INFO: Installation finished.
```

This password is stored in the `wazuh-passwords.txt` file in `wazuh-install-files.tar`.It can also be seen by running

```shell
tar -O -xvf wazuh-install-files.tar wazuh-install-files/wazuh-passwords.txt
```

### Testing

We can now test the whole setup by logging in to the dashboard.
Access it by going to `https:<dashboard-ip>` and using the admin credentials seen above

# Replicated

network example:

```
              +-------------------+    +------------+
              | server-1 <master> |    | - client-1 |
Dashbord ---- | server-2          | -- |            | 
      |       | server-3          |    | - client-2 |
      |       +-------------------+    |            |
      |                 |              | - client-3 |
      |       +-------------------+    |            |
      |       | indexer-1         | -- | - client-4 |
      + ----- | indexer-2         |    +------------+
              | indexer-3         |
              +-------------------+


indexer-N   : 10.0.0.1N
server-N    : 10.0.0.2N
dashboard : 10.0.0.30

clients-N  : 10.0.0.N
```

since there are now many possible machines where commands could be run from, there will be annotations on where to run the from like so:

```shell
# ls in indexer-1
indexer-1 $ ls
# ls in any indexer 
indexer-any $ ls
# ls in ALL indexeres
indexer-all $ ls
```

## Indexer

### Install

These first steps can be run in any indexer, as long as its allways the same one, for the example commands indexer-1 will always be chosen

- Download the installation assistant and configuration file

```shell
indexer-1 $ curl -sO https://packages.wazuh.com/4.3/wazuh-install.sh
indexer-1 $ curl -sO https://packages.wazuh.com/4.3/config.yml
```

- Edit the config file `config.yml` and replace the node ips
- the server nodes need to have a `node_type`, only one of which can have type master.
Ex.: config

```
nodes:
  indexer:
    - name: node-1
      ip: 10.0.0.11
    - name: node-2
      ip: 10.0.0.12
    - name: node-3
      ip: 10.0.0.13
  server:
    - name: wazuh-1
      ip: 10.0.0.21
      node_type: master
    - name: wazuh-2
      ip: 10.0.0.22
      node_type: worker 
    - name: wazuh-3
      ip: 10.0.0.23
      node_type: worker 
  dashboard:
    - name: dashboard
      ip: 10.0.0.30
```

- run the downloaded script with `--generate-config-files` to generate the cluster config files/passwords/keys
- this opperations needs write permissions to `/var/log` so run as su with `sudo`

```shell
indexer-1 $ bash wazuh-install.sh --generate-config-files
```

- copy `wazuh-install-files.tar` to every node that will be an inder/server/dashboard. The file will be created with a low permisison level (600) so we need to copy it as `root`.
**[Note]** since the user `root` has its own home, its ssh key related files ate stored in /home/.ssh . If using `scp` be sure that the user root can use `ssh` to log in to the target machines ( in this case, the server and dashboard).
**[Note]** The `.tar` file should not user readable since it contains sensitive information.

```shell
indexer-1 $ sudo bash wazuh-install.sh --wazuh-indexer node-1
```

- Now install the indexer on the other machines by first dowloading the script, and then running it.

```shell
indexer-all $ curl -sO https://packages.wazuh.com/4.3/wazuh-install.sh
indexer-all $ sudo bash wazuh-install.sh --wazuh-indexer node-N
```

- finally start the cluster in any node

```shell
indexer-any $ sudo bash wazuh-install.sh --start-cluster
```

### Testing

```shell
indexer-any $ tar -axf wazuh-install-files.tar wazuh-install-files/wazuh-passwords.txt -O | grep -P "\'admin\'" -A 1
```

the above command fetches the admin password,
replace `<ADMIN_PASSWORD>` by the password just fetched and `<WAZUH_INDEXER_IP>` by the server ip.

```shell
indexer-any $ curl -k -u admin:<ADMIN_PASSWORD> https://<WAZUH_INDEXER_IP>:9200
```

- this should result in somehting like this:

```json
{sd
  "name" : "node-1",
  "cluster_name" : "wazuh-cluster",
  "cluster_uuid" : "cMeWTEWxQWeIPDaf1Wx4jw",
  "version" : {
    "number" : "7.10.2",
    "build_type" : "rpm",
    "build_hash" : "e505b10357c03ae8d26d675172402f2f2144ef0f",
    "build_date" : "2022-01-14T03:38:06.881862Z",
    "build_snapshot" : false,
    "lucene_version" : "8.10.1",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "The OpenSearch Project: https://opensearch.org/"
}
```

## Server

### Install

- For the server the only thing needed is to download the install script and run it
  
**[Note]** the `wazuh-install-files.tar` needs to be copied to the Server if it hasn't been copied before

- The node name needs to be the same as configured in config.yml.

For the example config it would be `wazuh-1` as used bellow:

```shell
server-all $ curl -sO https://packages.wazuh.com/4.3/wazuh-install.sh
server-all $ bash wazuh-install.sh --wazuh-server wazuh-N
```

## Dashboard

### Install

- For the dashboard we have the same considerations as with the server, so do the same steps but when running the install script run with `--wazuh-dashboard` and the dashboard node name.

**[Note]** the `wazuh-install-files.tar` needs to be copied to the Server if it hasn't been copied before

```shell
dashboard $ curl -sO https://packages.wazuh.com/4.3/wazuh-install.sh
dashboard $ sudo bash wazuh-install.sh --wazuh-dashboard dashboard
```

After this is done the bellow text should be printed. The `<ADMIN_PASSWORD>` can be used to log in to the wazuh dashboard.

```
INFO: --- Summary ---
INFO: You can access the web interface https://<wazuh-dashboard-ip>
   User: admin
   Password: <ADMIN_PASSWORD>

INFO: Installation finished.
```

The password is stored in the `wazuh-passwords.txt` file in `wazuh-install-files.tar`. It can also be seen by running

```shell
dashboard $ tar -O -xvf wazuh-install-files.tar wazuh-install-files/wazuh-passwords.txt
```

### Testing

We can now test the whole setup by logging in to the dashboard.
Access it by going to `https:<dashboard-ip>` and using the admin credentials seen above

# Clients

There are roughly 3 steps to configuring and deploying clients
First the shared configurations should be set. Then thte client must be installed. Finally we can make user specific configurations since the client can automaticly update the config from the server these steps do not need to be executed in any specific order.

## Client deployment

The easiest way to deploy a client is through the dashboard. This process is explained in the [official documentatiton](https://documentation.wazuh.com/current/installation-guide/wazuh-agent/index.html)
In the tab: `Wazuh>gents` there is a `Deploy new agent` button. This will open a page with instructions to deploy a new agent for every suported system, just sellect the target OS, chose the groups to give the agent and run the shown commands.
The first box shows commands that download, install and configure ossec.
The second box is to enable ossec to run as daemon.

# Configuration

Each node is part of an user group. Whenever the ossec daemon runs it first reads the local config, then reads the server config file wich is the one for whatever group it is in. All groups have their own directory inside /var/ossec/etc/shared in the master server filesystem. All useres belong to the default group bur can be added to any number of them. They configuration files are loaded in the order : `local => remote group1 => remote group 2 ...`
each time a new configuration file is loaded, if they dont agree, the lsat one read will be the one considered. This means that server configs Always superceed local ones.
Each  grop also has its own rootit/malware detection files.
A samble tree for the `shared/` directory is:

```
/var/ossec/etc/shared/
├── ar.conf
├── debian
│   ├── agent.conf
│   ├── merged.mg
│   ├── rootkit_files.txt
│   ├── rootkit_trojans.txt
 ...
│   ├── win_audit_rcl.txt
│   └── win_malware_rcl.txt
└── default
    ├── agent.conf
    ├── merged.mg
    ├── rootkit_files.txt
    ├── rootkit_trojans.txt
   ...
    ├── win_audit_rcl.txt
    └── win_malware_rcl.txt
```

## [Main configurations options](#<https://documentation.wazuh.com/current/user-manual/reference/ossec-conf/syscheck.html)>

To configure what files are checked and how we can user the `<systemchck>` tag, Inside of wich there can be configured what directories to monitor with `<directories>` and those can be configured using other tag
Example:

```xml
<syscheck>
  <alert_new_files>yes</alert_new_files>
  <frequency>36000</frequency>
  <directories>/usr /home/*/Downloads </directories>
  <directories realtime="yes">/bin</directories>
<syscheck>
```

this snipet tells ossec to run a syscheck every 10h (36000s), which checks files in `/usr`, `/Downloads` for every user, and `/bin` in realtime. In all of these directories new files will also trigger an alert.

### Sheduling scans

- System check [frequency](https://documentation.wazuh.com/current/user-manual/reference/ossec-conf/syscheck.html#reference-ossec-syscheck-frequency); value in seconds, this example is mento to repeat every 10 hours

```xml
<systemchck>
  <frequency>360000</frequency>
<systemchck>
```

- alternativly can be set to run at [set times](https://documentation.wazuh.com/current/user-manual/reference/ossec-conf/syscheck.html#reference-ossec-syscheck-scan-time) on [set days](https://documentation.wazuh.com/current/user-manual/reference/ossec-conf/syscheck.html#reference-ossec-syscheck-scan-day)

```xml
<syscheck>
  <scan_time>10pm</scan_time>
  <scan_day>saturday</scan_day>
</syscheck>
```

### Real time monitoring

Real time monitoring can be set up for directories ( not for single files ) using the `realtime=yes` tag like so

```xml
<syscheck>
    <directories realtime="yes">/bin</directories>
</syscheck>
```

### Alerting on new files

```xml
<syscheck>
  <alert_new_files>yes</alert_new_files>
<syscheck>
```

## [Active-response](https://documentation.wazuh.com/current/user-manual/capabilities/active-response/how-it-works.html)

Ossec can be configured to perform specified actions whenever a certain condition is met. This is done with the '<active-response>' tag that will trigger a '<command>' and these, in turn can be specified to run shell scripts. Active-response needs to be set in the manager's config and will execute the the script saved in the machine that needs to run the comand. In the agent config the only valid entries are `<disabled>` ( which should be set to `no`) and `<repea_offenders>` which created a grace period in which repeated rules cannot trigger.
example.:
this script saved in `/var/ossec/active-response/bin/pwd.sh` which is the directory ossec checks for active command executables

```bash
#!/bin/sh
# create a file with the contents of the pwd command in ~/pwdtxt
pwd > ~/pwdtxt
```

would be triggered by these 2 snippets inside the `<syscheck>` configuration

```xml
<syscheck>
  ...
  <!-- Manager config  -->
  <!-- Active response -->
  <active-response>
    <disabled>no</disabled>
    <command>pwdcmd</command>
    <level>5</level>
  </active-response>

  <command>
    <name>pwdcmd</name>
    <executable>pwd.sh</executable>
  </command>


  <!-- Agent config  -->

  <active-response>
    <disabled>no</disabled>
    <repeat_offnders>1,5,10</repeat_offenders>
  </active-response>
  ...
</syscheck>
```

The `<executable>` tag's value is the name of the file that will be executed, it needs to have execute permitions, and should be in the user group `wazug`. These files do not sync with the server automaticly and should be copyed there manualy.

### Main configuration points

It's possible to configure where to run the scrips with the `<locations>` tag. It accepts the following values:

- `local` > executes in the machine that triggered the alert
- `server`> executes in the machine that triggered the alert
- `all`   > executes in the machine that triggered the alert
- `defined-user`> used in conjunction with the `<agent_id>` tag to specify the exact agent in wich to run the command
`<command>`

To specify when to execute the command use any combination of these, note that all must bemet:

- `<level>` minimum severity level to trigger
- `<rules_group>` rule group that a rule must belong to
- `<rules_id>` limits execution to when one of these fires

For statefull commands, a timeout must be set before the reverse command is executed.

- `<timeout>` gets a number of seconds until restoring the "normal" state

## Client specific configuation

If some client needs aditional configuration that is not covered in the shared config the ossec.conf file can be eddited to reflect these changes. Note that if these two have diferent values for the same option, the servers value will be take in to account.
Instead of changing the local config, conditionals can be placed in the shared config so that some rules only aply to some select agents. There is also the option to ignore the shared configuration. If the shared config is altered it will be set to the agent in the next keepalive, if the `auto_restart` option has been set to `no` ossec will need to be restarted manualy.
In the server run `/var/ossec/bin/agent_control -R -u <agent-id>` to restart.

## [Testing](https://documentation.wazuh.com/4.3/user-manual/agents/agent-connection.html)

In the master server run
`$ shell /var/ossec/bin/agent_control -i <YOUR_AGENT_ID> | grep Status` to show all registered agents
